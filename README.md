# Auto Complete #
Java program for returning a list of matching strings by highest weight. 

## Implementation ##

- *BruteAutocomplete* - Brute force implementation. Can be run using the `BruteAutocompleteClient`, a simple command line interface. 
- *QuickAutocomplete* - Implements binary search to improve performance. Can be run using `QuickAutocompleteClient` or `TheServer`, a small web app using a Java Servlet with JQuery on the frontend. 


## Testing Strategy ##
Classes tested include: `QuickAutocomplete`, `BruteAutocomplete` and `Term`, with the majority implemented using the TDD method. 

Testing strategy consists of three steps:

* Testing for valid, legal input/parameters
* Testing ‘Edge’ / ‘Borderline’ cases (i.e., long strings, illegal characters, etc)
* Illegal input/parameters, testing that all relative exceptions were thrown 

A small data file of 84 terms was used to facilitate the tests. 